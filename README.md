# CI/CD with Azure DevOps
Lab 03: Deploying build artifacts to Azure Artifacts

---

## Preparation

 - Import the repository (from Azure Repos):

```
https://oauth2:u9XWV6qNpU7z9PzWRHF5@gitlab.com/build-release-vsts/npm-demo-app.git

```
<img alt="Image artifacts-02" src="images/repo_import.jpeg"  width="75%" height="75%">
## Instructions


### Create a Universal Repository in Azure Artifacts

 - Create a new artifacts feed
 

<img alt="Image artifacts-01" src="images/artifacts-01.PNG"  width="75%" height="75%">
 
 
 - Call it "**my-feed**" and click "Create"
 

<img alt="Image artifacts-02" src="images/artifacts-02.PNG"  width="75%" height="75%">


### Create a Build Definition

 - Browse to the "**Pipelines**" page and click the button "New pipeline"
 
 - Click on use classic editor

 <img alt="Image artifacts-05" src="images/classic_editor.png"  width="75%" height="75%">

 - Click on continue

 - Select the "Empty Job" template

  ![Image 3](images/lab03-3.png)

 - Select the imported repository as build sources (npm-demo-app)

 <img alt="Image artifacts-05" src="images/lab03-15.PNG"  width="75%" height="75%">

 - Click on continue
 - Go to the Trigger tab
 - Configure CI Trigger:

```
Enable continuous integration
```

  ![Image 5](images/lab03-5.png)
  
 - Go to the options tab
 - Set the build number format: 

```
npm-app_$(Date:yyyyMMdd)$(Rev:.r)
```

<img alt="Image artifacts-03" src="images/artifacts-03.PNG"  width="75%" height="75%">
 
 - Go to the tasks tab 
 - Select Agent Pool:

```
my-pool
```

<img alt="Image artifacts-04" src="images/artifacts-04.PNG"  width="75%" height="75%">

 - Add a "**npm**" Task:

```
Display Name: npm install
Command: install
Working folder with package.json: $(Build.SourcesDirectory)
```

  ![Image 8](images/lab03-8.png)
  
 - Add a "**npm**" Task:

```
Display Name: Test
Command: custom
Working folder with package.json: $(Build.SourcesDirectory)
Command and arguments: run test
```

  ![Image 9](images/lab03-9.png)
  
 - Add a "**npm**" Task:

```
Display Name: Build
Command: custom
Working folder with package.json: $(Build.SourcesDirectory)
Command and arguments: run build
```

  ![Image 10](images/lab03-10.png)
  
 - Add a "**Copy Files**" Task:

```
Display Name: Stage Artifacts
Source Folder: $(Build.SourcesDirectory)
Contents: *.zip
Target Folder: $(build.artifactstagingdirectory)
```

  ![Image 11](images/lab03-11.png)

  
 - Add a "**Universal Packages**" Task:

```
Display Name: Upload Artifact
Command: Publish
Path to file(s) to publish: $(Build.ArtifactStagingDirectory)\
Feed location: This organization/collection
Destination Feed: my-feed
Package name: my-artifact
Version: Next patch
```

<img alt="Image artifacts-05" src="images/artifacts-05.PNG"  width="75%" height="75%">


 - Save and queue the build

---

### Create a Build Definition using YAML syntax (Optional)

- Now, let's create the same build definition using YAML syntax:
 
```
# Node.js
# Build a general Node.js project with npm.
# Add steps that analyze code, save build artifacts, deploy, and more:
# https://docs.microsoft.com/azure/devops/pipelines/languages/javascript

trigger:
- master

pool:
  name: 'my-pool'

steps:
- task: NodeTool@0
  inputs:
    versionSpec: '10.x'
  displayName: 'Install Node.js'

- script: |
    npm install
    npm run tests
    npm run build
  displayName: 'npm install, test and build'

# Copy Files
- task: CopyFiles@2
  inputs:
    sourceFolder: $(Build.SourcesDirectory)
    contents: '*.zip' 
    targetFolder: $(Build.ArtifactStagingDirectory)

- task: UniversalPackages@0
  displayName: Universal Publish
  inputs:
    command: publish
    publishDirectory: '$(Build.ArtifactStagingDirectory)'
    vstsFeedPublish: 'my-feed'
    vstsFeedPackagePublish: 'my-artifact-yaml'
    packagePublishDescription: 'my-artifact description'
```
